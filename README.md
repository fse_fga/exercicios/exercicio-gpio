# Exercício - GPIO - Entradas e Saídas Digitais

Exercício de acionamento das Saídas Digitais (GPIO) utilizando a Raspberry Pi

## 1 - OBJETIVOS

Este exercício tem o objetivo de efetuar o controle das **entradas** e **saídas** digitais (GPIO).

A escrita nas **saídas** serão realizadas tanto no modo ***liga/desliga*** quanto no modo ***PWM***.

A leitura das **entradas** serão realizadas em modo ***pooling***, ***eventos*** e ***interrupções***. 

Os exercícios podem ser realizados tanto em linguagem ***Python*** quanto em ***C/C++***.

Os pinos da GPIO da Raspberry Pi tem funções de propósito geral (Entrada / Saída) ou propósitos específicos de acordo com o protocolo de comunicação desejado.

A figura abaixo ilustra a designação padrão de cada pino. Além dos pinos de GPIO, há pinos com propósitos específicos como:

- **3v3 Power** que fornece tensão de 3.3V;  
- **5v Power** que fornece tensão de 5V;
- **Ground** ou **GND** que que é a referência de tensão ou tensão 0V.       

![Figura RPI3 GPIO](imagens/Rpi3_GPIO_Mapa.png)   
Referência: https://pinout.xyz/

## 2 - Montagem do Circuito

### 2.1 Circuito com 1 LED e 1 Botão

Para iniciar, é necessário ter em mãos:

1. Placa Raspberry Pi 3/4  
2. Protoboard  
3. Fios (Jumpers)  
4. Led
5. Resistor
6. Botão (Push button) ou Jumper

Faça a montagem conforme sugerido na figura abaixo:

![Figura 1](imagens/Protoboard_RPI3_Led_Button.png)

**Atenção**: **NUNCA** ligue o LED diretamente na energia sem um resistor. O resistor é obrigatório para limitar a corrente. Caso contrário, o LED vai queimar. O resistor a ser usado precisa ser de no mínimo 220 ohms.

### 2.2 - Montagem do Circuito com 1 LED RGB e 1 Botão

Para iniciar, é necessário ter em mãos:

1. Placa Raspberry Pi 3/4  
2. Protoboard  
3. Fios (Jumpers)  
4. Led RGB (Já inclui resistores)
5. Botão (Push button) ou Jumper

Faça a montagem conforme sugerido na figura abaixo:

![Figura 2](imagens/esquematico_led_rgb.png)

### 2.3 - Montagem do Circuito com 1 LED RGB e 1 Encoder

Para iniciar, é necessário ter em mãos:

1. Placa Raspberry Pi 3/4  
2. Protoboard  
3. Fios (Jumpers)  
4. Led RGB (Já inclui resistores)
5. Encoder (KY-040)

Faça a montagem conforme sugerido na figura abaixo:

![Figura 2](imagens/Montagem_RGB_Encoder.png)

## 3 - Exercícios 

Os códigos de exemplo estão disponíveis no repositório [GPIO Raspberry Pi](https://gitlab.com/fse_fga/gpio-raspberry-pi).

### Exercício 1 - Liga/Desliga

Neste exercício a porta GPIO deve ser ativada como saída digital.

A) Acionar o LED ligando e desligando;  
B) Acionar o LED em modo liga/desliga temporizado.

### Exercício 2 - PWM

Neste exercício deve ser usada a técnica do PWM simulado por software (SoftPWM)

A) Acionar um LED individualmente oscilando sua intensidade de 0% a 100% e em seguida de 100% a 0% em uma curva acendente e descendente linear;

### Exercício 3 - Entradas em Polling

A) Utilizar a técnica de polling para ler o estado de um botão (ou jumper) e imprimir na tela cada mudança de estado;  
B) Utilizar a técnica de debounce para filtrar as mudanças de estado espúrias do botão;  

### Exercício 4 - Eventos e Interrupções

A) Utilizar o recurso de monitorar eventos para ler o estado de um botão;   
B) Utilizar as interrupções para monitorar a mudança de estado de um botão;  
C) Utilizando eventos ou interrupções, tratar o problema de bouncing para ler o estado de uma entrada.

### Exercício 5 - Leitura do Encoder

A) Realizar a montagem do item 2.3 com um encoder KY-040;  
B) Realizar a leitura do botão SW do encoder;  
C) Realizar a leitura dos valores do encoder e imprimir em tela (printf) a posição atual sem perda de passos.  

## 4 - Referências

### Bibliotecas em Python

- gpiozero (https://gpiozero.readthedocs.io)
- RPi.GPIO (https://pypi.org/project/RPi.GPIO/)

A documentação da RPi.GPIO se encontra em
https://sourceforge.net/p/raspberry-gpio-python/wiki/Examples/

### Bibliotecas em C/C++

- WiringPi (https://github.com/WiringPi/WiringPi/tree/master)
- WiringPi - Documentação Gordons Projects (https://projects.drogon.net/raspberry-pi/wiringpi/)
- BCM2835 (http://www.airspayce.com/mikem/bcm2835/)
- PiGPIO (http://abyz.me.uk/rpi/pigpio/index.html)
- sysfs (https://elinux.org/RPi_GPIO_Code_Samples)

### Datasheet Encoder KY-040

- KY-040 (https://www.handsontec.com/dataspecs/module/Rotary%20Encoder.pdf)  

### Lista de Exemplos de acesso à GPIO com múltiplas linguagens

Há um compilado de exemplos de acesso à GPIO em várias linguages de programação como C, C#, Ruby, Perl, Python, Java e Shell (https://elinux.org/RPi_GPIO_Code_Samples).
